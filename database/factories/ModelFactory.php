<?php

use Faker\Generator as Faker;
use App\User;
use App\Voter;
use App\Candidate;
use App\Vote;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});

$factory->define(Voter::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastname,
        'dni' => (string) $faker->randomNumber(8, true),
        'birthday' => $faker->date,
        'address' => $faker->streetAddress,
        'status' => 0,
        'email' => $faker->email,
    ];
});

$factory->define(Candidate::class, function (Faker $faker) {
    $politicalParties = ['Partido Federal', 'Union Civica Radical', 'Union Popular', 'Partido Socialista'];
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastname,
        'birthday' => $faker->date,
        'address' => $faker->streetAddress,
        'dni' => (string) $faker->randomNumber(8, true),
        'email' => $faker->email,
        'list' => 'Lista ' . $faker->randomDigit,
        'political_party' => $politicalParties[array_rand($politicalParties)],
        'image' => 'default.jpg',
    ];
});

$factory->define(Vote::class, function (Faker $faker) {
    $candidate = Candidate::all()->pluck('id');
    return [
        'vote' => '1',
        'id_candidate' => $candidate->random(),
    ];
});