<?php

use Illuminate\Database\Seeder;
use App\Vote;
use App\Candidate;

class VoteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Vote::class, 20)->create();
        $candidateId = Candidate::whereFirstName('WhiteVote')->first()->id;
        Vote::create([
            'vote' => '1',
            'id_candidate' => $candidateId,
        ]);
    }
}
