<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(VoterTableSeeder::class);
        $this->call(CandidateTableSeeder::class);
        $this->call(WhiteCandidateSeeder::class);
        $this->call(VoteTableSeeder::class);
    }
}
