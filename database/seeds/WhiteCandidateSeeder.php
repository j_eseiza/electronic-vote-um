<?php

use Illuminate\Database\Seeder;
use App\Candidate;

class WhiteCandidateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $candidate = Candidate::whereFirstName('WhiteVote')->get();
        if ($candidate->isEmpty()) {
            Candidate::create([
                'first_name' => 'WhiteVote',
                'last_name' => 'White',
                'dni' => '00000000',
                'birthday' => '1996-01-26',
                'address' => 'White',
                'email' => 'example@white.com',
                'list' => 'White',
                'political_party' => 'White',
            ]);
        }
    }
}
