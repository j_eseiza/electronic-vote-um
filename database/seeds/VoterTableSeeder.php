<?php

use Illuminate\Database\Seeder;
use App\Voter;

class VoterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Voter::class, 50)->create();
    }
}
