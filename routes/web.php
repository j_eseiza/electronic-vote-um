<?php
/*
 *
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Home */
Route::get('/', 'PrincipalController@index');
Route::post('/votar', 'PrincipalController@createVoteAndUpdateStatusVoter')->name('principal.createVoteAndUpdateStatusVoter');
Route::post('/buscar', 'PrincipalController@voterSearch')->name('principal.voterSearch');

/* Voter Resource*/
Route::resource('votante', 'VoterController')->except(['destroy']);
Route::post('/votante/buscar', 'VoterController@searchByFilter')->name('voters.searchByFilter');

/*Candidate Resource*/
Route::resource('candidato', 'CandidateController')->except(['show']);
Route::get('/candidatos/listar', 'CandidateController@listCandidates')->name('candidate.list');
Route::post('/candidato/buscar','CandidateController@searchByFilter')->name('candidate.searchByFilter');

/* Authentication Routes*/
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

/* Last View Route*/
Route::get('/ganador', 'CandidateController@searchWinnerCandidate')->name('candidate.searchWinner');
