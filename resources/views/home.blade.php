@extends('layouts.app')

@section('content')
    <div class="d-flex justify-content-center">
        <h1>
            <span class="badge badge-info">
                Bienvenido {{ Auth::user()->name }}
            </span>
        </h1>
    </div>
    <div id="chart-div" class="d-flex justify-content-center">
        <?= \App\Charts\Chart::getInstance()->getBarChartCandidate()
            ->render('BarChart', 'Estadisticas de votos', 'chart-div')?>
    </div>
    <br>
    <div class="d-flex justify-content-center">
    @if (session('voting_status') === null)
        <a class="btn btn-dark"
           href="{{ route('candidate.searchWinner') }}">
            Cerrar Votación
        </a>
        @else
            <a class="btn btn-dark disabled"
               href="#">
                Cerrar Votación
            </a>
    @endif
    </div>
@endsection