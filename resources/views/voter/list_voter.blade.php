@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        {{--Navbar de busqueda y crear votante--}}
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand">Votantes</a>
            <ul class="navbar-nav">
                <li>
                    <form class="form-inline my-2 my-lg-0" method="post" action="{{route('voters.searchByFilter')}}">
                        @csrf
                        <input class="form-control mr-sm-2" type="search" placeholder="Buscar" aria-label="Search" name="search">
                        <div class="input-group mr-sm-2">
                            <select class="custom-select" id="inputGroupSelect01" name="filter">
                                <option selected>Filtro</option>
                                <option value="first_name">Nombre</option>
                                <option value="last_name">Apellido</option>
                                <option value="dni">DNI</option>
                            </select>
                        </div>
                        <button class="btn btn-outline-info my-2 my-sm-2" type="submit">Buscar</button>
                    </form>
                </li>
                <li>
                    <div class="form-inline my-sm-2">
                        <a class="btn btn-success float-right ml-sm-2" href="{{ route('votante.create')}}">
                            Agregar
                        </a>
                    </div>
                </li>
            </ul>
        </nav>
        {{--Tabla de Votantes--}}
        <table class="table">
            <thead class="thead-light">
            <tr>
                <th scope="col">Nombre </th>
                <th scope="col">Apellido </th>
                <th scope="col">Fecha de Nacimiento</th>
                <th scope="col">DNI</th>
                <th scope="col">Direccion</th>
                <th scope="col">Email</th>
                <th></th>
            </tr>
            </thead>

            @foreach ($voters as $voter)
                <tr>
                    <td>{{ $voter->first_name }}</td>
                    <td>{{ $voter->last_name }}</td>
                    <td>{{$voter->birthday}}</td>
                    <td>{{ $voter->dni }}</td>
                    <td>{{$voter->address}}</td>
                    <td>{{$voter->email}}</td>
                    <td><a class="btn btn-warning" href="{{ route('votante.edit', $voter->id) }}">Editar</a></td>
                </tr>
            @endforeach
        </table>
        {{ $voters->links() }}
    </div>
@endsection
