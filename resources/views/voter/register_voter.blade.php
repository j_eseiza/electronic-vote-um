@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <h1>Votante</h1>
        <hr style="background-color: #1b1e21">
        @if (session()->has('age'))
            <div class="alert alert-danger" role="alert">
                {{ session('age') }}
            </div>
        @endif
        <form action="{{ route('votante.store') }}" method="POST">
            @csrf
            <label for="">
                Nombre
                <input class="form-control" type="text" name="first_name" value="{{ old('first_name') }}">
                {!! $errors->first('first_name','<span class="text-danger error">:message</span>') !!}
            </label>
            <br>
            <label for="">
                Apellido
                <input class="form-control" type="text" name="last_name" value="{{ old('last_name') }}">
                {!! $errors->first('last_name','<span class="text-danger error">:message</span>') !!}
            </label>
            <br>
            <label for="">
                Fecha de nacimiento
                <input class="form-control" type="date" name="birthday" value="{{ old('birthday') }}">
                {!! $errors->first('birthday','<span class="text-danger error">:message</span>') !!}
            </label>
            <br>
            <label for="">
                Dni
                <input class="form-control" type="text" name="dni" value="{{ old('dni') }}">
                {!! $errors->first('dni','<span class="text-danger error">:message</span>') !!}
            </label>
            <br>
            <label for="">
                Dirección
                <input class="form-control" type="text" name="address" value="{{ old('address') }}">
                {!! $errors->first('address','<span class="text-danger error">:message</span>') !!}
            </label>
            <br>
            <label for="">
                Email
                <input class="form-control" type="email" name="email" value="{{ old('email') }}">
                {!! $errors->first('email', '<span class="text-danger error">:message</span>') !!}
            </label>
            <br>
            <input class="btn btn-primary" type="submit" value="{{ 'Guardar' }}">
            <a class="btn btn-danger" href="{{ route('votante.index') }}">Cancelar</a>
        </form>
        <hr>
    </div>
@endsection