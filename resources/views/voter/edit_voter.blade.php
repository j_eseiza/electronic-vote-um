@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <h1> Votante </h1>
        <hr style="background-color: #1b1e21">
        <form action="{{ route('votante.update',$voter->id) }}" method="POST">
            @csrf
            @method('patch')
            <br>
            <label for="">
                Nombre
                <input class="form-control" type="text" name="first_name" value="{{$voter->first_name}}">
                {!! $errors->first('first_name','<span class=error>:message</span>') !!}
            </label>
            <br>
            <label for="">
                Apellido
                <input class="form-control" type="text" name="last_name" value="{{$voter->last_name}}" >
                {!! $errors->first('last_name','<span class=error>:message</span>') !!}
            </label>
            <br>
            <label for="">
                Fecha de nacimiento
                <input class="form-control" type="date" name="birthday" value="{{$voter->birthday}}" >
                {!! $errors->first('birthday','<span class=error>:message</span>') !!}
            </label>
            <br>
            <label for="">
                Dni
                <input class="form-control" type="text" name="dni" value="{{$voter->dni }}" >
                {!! $errors->first('dni','<span class=error>:message</span>') !!}
            </label>
            <br>
            <label for="">
                Direccion
                <input class="form-control" type="text" name="address" value="{{$voter->address }}" >
                {!! $errors->first('address','<span class=error>:message</span>') !!}
            </label>
            <br>
            <label for="">
                Email
                <input class="form-control" type="email" name="email" value="{{$voter->email }}" >
                {!! $errors->first('email', '<span class=error>:message</span>') !!}
            </label>
            <br>
            <input class="btn btn-primary" type="submit" value="{{ 'Guardar ' }}">
            <a class="btn btn-danger" href="{{ route('votante.index') }}">Cancelar</a>

        </form>
        <hr>
    </div>
@endsection