@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <h1>Candidato</h1>
        <hr style= "background-color: black;"/>
        <form action="{{route('candidato.update', $candidate->id) }}" enctype="multipart/form-data" method="POST" type="file">
            @csrf
            @method('patch')
            <label for="">
                Nombre
                <input class="form-control" type="text" name="first_name" value="{{$candidate->first_name}}">
                {!! $errors->first('first_name','<span class=text-danger error>:message</span>') !!}
            </label>
            <br>
            <label for="">
                Apellido
                <input class="form-control" type="text" name="last_name" value="{{$candidate->last_name}}">
                {!! $errors->first('last_name', '<span class=text-danger error>:message</span>') !!}
            </label>
            <br>
            <label for="">
                Fecha de nacimiento
                <input class="form-control" type="date" name="birthday" value="{{$candidate->birthday}}">
                {!! $errors->first('birthday', '<span class=error>:message</span>') !!}
            </label>
            <br>
            <label for="">
                DNI
                <input class="form-control" type="text" name="dni" value="{{$candidate->dni}}">
                {!! $errors->first('dni', '<span class=text-danger error>:message</span>') !!}
            </label>
            <br>
            <label for="">
                Direccion
                <input class="form-control" type="text" name="address" value="{{$candidate->address}}">
                {!! $errors->first('address', '<span class=text-danger error>:message</span>') !!}
            </label>
            <br>
            <label for="">
                Email
                <input class="form-control" type="email" name="email" value="{{$candidate->email}}">
                {!! $errors->first('email', '<span class=text-danger error>:message</span>') !!}
            </label>
            <br>
            <label for="">
                Lista
                <input class="form-control" type="text" name="list" value="{{$candidate->list}}">
                {!! $errors->first('list', '<span class=text-danger error>:message</span>') !!}
            </label>
            <br>
            <label for="">
                Partido Politico
                <select name="political_party">
                    @foreach($political_parties as $party)
                        @if($party == $candidate->political_party)
                            <option selected value="{{$party}}">{{$party}}</option>
                        @else
                            <option value="{{$party}}">{{$party}}</option>
                        @endif
                    @endforeach
                </select>
                {!! $errors->first('political_party', '<span class=error>:message</span>') !!}
            </label>
            <br>
            <label for="image">
                Imagen Perfil
                <img src="{{ asset('images/candidate/' . $candidate->image) }}" alt="" class="rounded" width="20%">
                <input type="file" class="form-control" name="image">
                {!! $errors->first('image', '<span class=text-danger error>:message</span>') !!}
            </label>
            <br>
            <input class="btn btn-primary" type="submit" value="{{ 'Guardar' }}">
            <a href="{{ route('candidato.index') }}" class="btn btn-danger">{{ 'Cancelar' }}</a>
        </form>
        <hr>
    </div>
@endsection