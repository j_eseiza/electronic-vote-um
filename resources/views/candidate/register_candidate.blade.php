@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <h1>Candidato</h1>
        <hr style= "background-color: black;"/>
        <form action="{{route('candidato.store')}}" enctype="multipart/form-data" method="POST" type="file">
            @csrf
            <label for="">
                Nombre
                <input class="form-control" type="text" name="first_name" value="{{ old('first_name') }}">
                {!! $errors->first('first_name','<span class=text-danger error>:message</span>') !!}
            </label>
            <br>
            <label for="">
                Apellido
                <input class="form-control" type="text" name="last_name" value="{{ old('last_name') }}">
                {!! $errors->first('last_name', '<span class=text-danger error>:message</span>') !!}
            </label>
            <br>
            <label for="">
                Fecha de nacimiento
                <input class="form-control" type="date" name="birthday" value="{{ old('birthday') }}">
                {!! $errors->first('birthday', '<span class=text-danger error>:message</span>') !!}
            </label>
            <br>
            <label for="">
                DNI
                <input class="form-control" type="text" name="dni" value="{{ old('dni') }}">
                {!! $errors->first('dni', '<span class=text-danger error>:message</span>') !!}
            </label>
            <br>
            <label for="">
                Direccion
                <input class="form-control" type="text" name="address" value="{{ old('address') }}">
                {!! $errors->first('address', '<span class=text-danger error>:message</span>') !!}
            </label>
            <br>
            <label for="">
                Email
                <input class="form-control" type="email" name="email" value=" {{ old('email') }}">
                {!! $errors->first('email', '<span class=text-danger error>:message</span>') !!}
            </label>
            <br>
            <label for="">
                Lista
                <input class="form-control" type="text" name="list" value="{{ old('list') }}">
                {!! $errors->first('list', '<span class=text-danger error>:message</span>') !!}
            </label>
            <br>
            <label for="">
                Partido Politico
                <select name="political_party">
                    @foreach($politicalParties as $party)
                        <option value="{{$party}}">{{$party}}</option>
                    @endforeach
                </select>
                {!! $errors->first('political_party', '<span class=text-danger error>:message</span>') !!}
            </label>
            <br>
            <label for="image">
                Imagen Perfil
                <input type="file" class="form-control" name="image" value="{{ old('image') }}">
                {!! $errors->first('image', '<span class=text-danger error>:message</span>') !!}
            </label>
            <br>
            <input class="btn btn-primary" type="submit" value="{{ 'Guardar' }}">
            <a class="btn btn-danger" href="{{ route('candidato.index') }}">Cancelar</a>
        </form>
        <hr>
    </div>
@endsection