@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        {{--Navbar de busqueda y crear candidato--}}
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand">Candidatos</a>
            <ul class="navbar-nav">
                <li>
                    <form class="form-inline my-2 my-lg-0" method="post" action="{{route('candidate.searchByFilter')}}">
                        @csrf
                        <input class="form-control mr-sm-2" type="search" placeholder="Buscar" aria-label="Search" name="param">
                        <div class="input-group mr-sm-2">
                            <select class="custom-select" id="inputGroupSelect01" name="filter">
                                <option selected>Filtro</option>
                                <option value="first_name">Nombre</option>
                                <option value="last_name">Apellido</option>
                                <option value="dni">DNI</option>
                            </select>
                        </div>
                        <button class="btn btn-outline-info my-2 my-sm-2" type="submit">Buscar</button>
                    </form>
                </li>
                <li>
                    <div class="my-sm-2">
                        <a class="btn btn-success float-right ml-sm-2" href="{{ route('candidato.create')}}">
                            Agregar
                        </a>
                    </div>
                </li>
            </ul>
        </nav>
        {{--Tabla de Candidatos--}}
        <table class="table">
            <thead class="thead-light">
            <tr>
                <th scope="col">Nombre</th>
                <th scope="col">Apellido</th>
                <th scope="col">Fecha de Nacimiento</th>
                <th scope="col">DNI</th>
                <th scope="col">Direccion</th>
                <th scope="col">Lista</th>
                <th scope="col">Partido Politico</th>
                <th scope="col">Email</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
                @foreach($candidates as $candidate)
                    <tr>
                        <td>{{$candidate->first_name}}</td>
                        <td>{{$candidate->last_name}}</td>
                        <td>{{$candidate->birthday}}</td>
                        <td>{{$candidate->dni}}</td>
                        <td>{{$candidate->address}}</td>
                        <td>{{$candidate->list}}</td>
                        <td>{{$candidate->political_party}}</td>
                        <td>{{$candidate->email}}</td>
                        <td><a class="btn btn-warning" href="{{ route('candidato.edit', $candidate->id) }}">Editar</a></td>
                        <td>
                            <form style="display:inline;" action="{{ route('candidato.destroy', $candidate->id) }}" method="POST">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger" type="submit">Eliminar</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
            </tbody>
        </table>
    </div>
@endsection