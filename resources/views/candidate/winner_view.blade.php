@extends('layouts.app')
@section('content')
    <div class="text-center">
        <h1>Candidato Electo</h1>
    </div>
    <div class="container-fluid text-center">
        <div class="d-inline-block" style="width: 25rem;">
            <div class="m-4 pr-lg-5 pl-lg-5" >
                <div class="border">
                    <img class="card-img-top" src="{{ asset('images/candidate/' . $candidate->image) }}">
                    <div class="card-body text-left">
                        <h5 class="card-title">{{$candidate->first_name}} {{$candidate->last_name}}</h5>
                        <p class="card-text">{{$candidate->political_party}}</p>
                        <p class="card-text">{{$candidate->list}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="chart-div" class="d-flex justify-content-center">
        <?= \App\Charts\Chart::getInstance()
            ->getBarChartCandidate()
            ->render('BarChart', 'Estadisticas de votos', 'chart-div')?>
    </div>
@endsection