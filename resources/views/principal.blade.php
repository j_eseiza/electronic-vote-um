@extends('layouts.app')
    @section('content')
        <div id="main" class="container-fluid">
            {{-- Busqueda del votante --}}
            <div class="d-flex justify-content-center mt-2">
                <div class="jumbotron w-75" style="padding: 2rem 2rem !important;">
                    <h1 class="display-4 text-center">Votaciones 2018</h1>
                    <hr class="my-4">
                    <div class="d-flex justify-content-center">
                        <div class="form-group">
                            <input class="form-control"  name="dni" @keyup.delete="deleteVoter()" placeholder="Ingrese su DNI" v-model="voter_dni">
                        </div>
                        <div class="pl-3 pr-3">
                            <a @click.prevent="getVoter()" class= "btn btn-warning btn-lg btn-block">Consultar</a>
                        </div>
                    </div>
                    <div v-if="voterExist">
                        <hr class="my-4 w-75">
                        <div class="d-flex justify-content-around">
                            <p class="float-left">Nombre Completo: @{{ voter.first_name }} @{{ voter.last_name }}</p>
                            <p class="float-right">DNI: @{{ voter.dni }}</p>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Seccion de votar --}}
            <div v-for="candidate in candidates" v-if="!(candidate.first_name === 'WhiteVote')" class="d-inline-block" style="width: 25rem;">
                <div class="m-4 pr-lg-5 pl-lg-5" >
                    <div class="border">
                        <img class="card-img-top" :src="'images/candidate/' + candidate.image">
                        <div class="card-body">
                            <h5 class="card-title"><strong>@{{ candidate.first_name }} @{{ candidate.last_name}}</strong></h5>
                            <p class="card-text">@{{ candidate.political_party }}</p>
                            <p class="card-text">@{{ candidate.list}}</p>
                            <form action="{{ route('principal.createVoteAndUpdateStatusVoter') }}" method="POST">
                                @csrf
                                <input type="submit" class="btn btn-success btn-lg btn-block" value="Votar">
                                <input type="hidden" name="voter_id" :value="voter.id">
                                <input type="hidden" name="candidate_id" :value="candidate.id">
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Voto en blanco --}}
            <div v-else>
                <form action="{{ route('principal.createVoteAndUpdateStatusVoter') }}" method="POST">
                    @csrf
                    <div class="w-100 d-flex justify-content-center">
                        <input type="submit" class="btn btn-info btn-lg" style="width: 25rem;" value="Voto en blanco">
                    </div>
                    <input type="hidden" name="voter_id" :value="voter.id">
                    <input type="hidden" name="candidate_id" :value="candidate.id">
                </form>
            </div>
    </div>
        <div id="chart-div" class="d-flex justify-content-center mt-4">
            <?= \App\Charts\Chart::getInstance()->getBarChartCandidate()
                ->render('BarChart', 'Estadisticas de votos', 'chart-div')?>
        </div>
@stop
