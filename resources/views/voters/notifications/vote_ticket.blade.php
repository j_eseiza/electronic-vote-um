<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="{{ asset('css/ticket.css') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <title>Electronic UMVote</title>
</head>
<body>
    <div class="container-fluid">
        <div class="main-container d-flex justify-content-between">
            <div id="voter-data">
                <h5>{{ $voter->first_name }}</h5>
                <h6>{{ $voter->last_name }}</h6>
                <img class="image-qr"
                     src="{{ \Illuminate\Support\Facades\URL::asset('/images/ticket/qr.png') }}"
                     height="130">
            </div>
            <div class="contenedor">
                <h2 class="titulos" style="font-size: 80px">Boleta</h2>
                <h4 class="titulos" style="font-size: 40px">de voto electrónico</h4>
                <hr>
                <h6>UM-VOTE.COM.AR</h6>
                <p class="slogan">SISTEMA DE VOTO ELECTRÓNICO - UNIVERSIDAD DE MENDOZA</p>
            </div>
            <div id="banda-lateral">
                <img src="{{ \Illuminate\Support\Facades\URL::asset('/images/ticket/banda.png') }}"
                     alt="">
            </div>

        </div>
        <br>
        <a href="{{ url('/') }}" class="btn btn-info btn-sm float-left">Home</a>
        <button class="btn btn-info btn-sm float-right">Imprimir</button>
    </div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</body>
</html>