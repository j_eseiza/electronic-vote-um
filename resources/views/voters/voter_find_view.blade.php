    <div class="d-flex justify-content-center mt-2">
        <div class="jumbotron w-75" style="padding: 2rem 2rem !important;">
            <h1 class="display-4 text-center">Votaciones 2018</h1>
            <hr class="my-4">
            <div class="d-flex justify-content-center">
                {{--<form action="{{ route('principal.voterSearch') }}" method="POST">--}}
                    {{--@csrf--}}
                    <div class="form-group">
                        <input class="form-control"  name="dni" placeholder="Ingrese su DNI" :value="voter_dni">
                    </div>
                    <div class="pl-3 pr-3">
                        <button type="submit" @click.prevent="getVoter()" class= "btn btn-warning btn-lg btn-block">Consultar</button>
                    </div>
                {{--</form>--}}
            </div>
            {{--@if(isset($voter))--}}
                <hr v-if="voter" class="my-4 w-75">
                <div class="d-flex justify-content-around">
                        <p class="float-left">Nombre Completo: @{{ voter.first_name }} @{{ voter.last_name }}</p>
                        <p class="float-right">DNI: @{{ voter.dni }}</p>
                </div>
            {{--@endif--}}
        </div>
    </div>