new Vue({
    el: '#main',
    created: function() {
        this.getCandidates();
    },
    data: {
        candidates: [],
        voter: [],
        voter_dni: '',
        voterExist: false,
        errors: '',
    },
    methods: {
        getCandidates: function() {
            const urlCandidates = 'candidatos/listar';
            axios.get(urlCandidates).then(response => {
                this.candidates = response.data
            });
        },
        getVoter: function () {
            const urlVoter = '/buscar/';
            axios
                .post(urlVoter, {dni: this.voter_dni})
                .then(
                    response => { this.voter = response.data; this.voterExist = true },
                    error =>{
                        let errors = error.response.data.errors;
                        if (errors.dni) {
                            this.errors = errors.dni;
                            this.errors.forEach(function (value) {
                                toastr.error(value);
                            })
                        }
                        if (errors.message) { toastr.error(errors.message); }
                    }
                )
                .catch(error => {
                    //
                });

        },
        deleteVoter: function () {
            this.voter = [];
            this.voterExist = false;
        }
    }
});