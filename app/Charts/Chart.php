<?php

namespace App\Charts;

use App\Candidate;
use Faker\Factory;
use Khill\Lavacharts\Lavacharts;

class Chart
{
    /**
     * @var Lavacharts $lava
     */
    protected $lava;

    /**
     * @var Factory $factory
     */
    protected $factory;

    /**
     * @var null
     */
    static private $instance = null;

    private function __construct()
    {
        $this->lava = new Lavacharts();
        $this->factory = Factory::create();
    }

    public static function getInstance(): self
    {
        if (self::$instance === null){
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @return Lavacharts
     * @throws \Khill\Lavacharts\Exceptions\InvalidCellCount
     * @throws \Khill\Lavacharts\Exceptions\InvalidColumnRole
     * @throws \Khill\Lavacharts\Exceptions\InvalidColumnType
     * @throws \Khill\Lavacharts\Exceptions\InvalidLabel
     * @throws \Khill\Lavacharts\Exceptions\InvalidRowDefinition
     * @throws \Khill\Lavacharts\Exceptions\InvalidRowProperty
     */
    public function getBarChartCandidate(): Lavacharts
    {
        $candidatesVotes = Candidate::getVotesOfCandidateWithoutWhiteVote();
        $countWhiteVotes = Candidate::getCountWhiteVotes();
        $candidatesVotes[0]->countVotes += $countWhiteVotes[0]->countVotes;

        $data = $this->lava->DataTable();

        $data->addStringColumn('Candidato')
            ->addNumberColumn('Porcentaje')
            ->addRoleColumn('string', 'style')
            ->addRoleColumn('string', 'annotation');

        foreach ($candidatesVotes as $vote) {
            $data->addRow([
                $vote->first_name . $vote->last_name,
                ($vote->countVotes),
                $this->factory->hexColor,
                ($vote->countVotes * 100) / 100 .'%'
            ]);
        }

        $this->lava->BarChart('Estadisticas de votos', $data, config('lavacharts.config'));

        return $this->lava;
    }
}
