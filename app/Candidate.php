<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class Candidate extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'first_name','last_name','dni','birthday','address','email','political_party','list', 'image',
    ];

    protected $rules = [
        'first_name'=>'required|alpha',
        'last_name'=>'required|alpha',
        'birthday'=>'required|date',
        'dni'=>'required|digits_between:7,8|numeric|unique:candidates',
        'address'=>'required|string',
        'list'=>'required',
        'political_party'=>'required|string',
        'email'=>'required|email|unique:candidates',
        'image'=>'image',
    ];

    protected $messages = [
        'birthday.date'=> 'La fecha de nacimiento no es una fecha válida'
    ];

    public function getValidationRules(): array
    {
        return $this->rules;
    }

    public function getValidationMessages(): array
    {
        return $this->messages;
    }

    public function getAttribute($key)
    {
        return $this->attributes[$key];
    }

    public function setAttribute($key, $value)
    {
        $this->attributes[$key] = $value;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vote()
    {
        return $this->hasMany(Vote::class, 'id_candidate');
    }

    public function getPoliticalParties(): array
    {
        $politicalParties = new Collection();
        $parties = Candidate::all()
            ->pluck('political_party')
            ->unique();
        foreach ($parties as $party){
            if ($party === 'White') { continue; }
            $politicalParties->push($party);
        }

        return $politicalParties->toArray();
    }

    public static function getVotesOfCandidate(): array
    {
        return DB::table('candidates')
            ->join('votes', 'candidates.id', '=', 'votes.id_candidate')
            ->select('candidates.first_name','candidates.last_name', DB::raw('count(votes.vote) as recuento'))
            ->groupBy('candidates.id')
            ->get()->toArray();
    }
    
    public static function getVotesOfCandidateWithoutWhiteVote(): array
    {
        return DB::table('candidates')
            ->join('votes', 'candidates.id', '=', 'votes.id_candidate')
            ->select('candidates.first_name','candidates.last_name','candidates.id',
                DB::raw('count(votes.vote) as countVotes'))
            ->where('candidates.last_name', '<>', 'White')
            ->where('candidates.deleted_at', null)
            ->groupBy('candidates.id')
            ->orderBy('countVotes', 'desc')
            ->get()->toArray();
    }

    public static function getCountWhiteVotes(): array
    {
        return DB::table('candidates')
            ->join('votes', 'candidates.id', '=', 'votes.id_candidate')
            ->select('candidates.first_name','candidates.last_name',
                DB::raw('count(votes.vote) as countVotes'))
            ->where('candidates.last_name', 'White')
            ->groupBy('candidates.id')
            ->get()->toArray();
    }

    public function getWinnerCandidate(): \stdClass
    {
        $candidate = self::getVotesOfCandidateWithoutWhiteVote()[0];
        $whiteVotes = self::getCountWhiteVotes()[0];
        $candidate->countVotes += $whiteVotes->countVotes;

        return $candidate;
    }
}
