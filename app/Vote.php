<?php

namespace App;

use App\Repositories\Repository;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Vote
 *
 * @property int $id
 * @property string $vote
 * @property int $id_candidate
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vote whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vote whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vote whereIdCandidate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vote whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vote whereVote($value)
 * @mixin \Eloquent
 * @property int $id_voter
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Vote whereIdVoter($value)
 */
class Vote extends Model implements \SplSubject /* Subject */
{
    private $storage;

    private $voteRepository;

    protected $fillable = ['vote', 'id_candidate', 'id_voter'];

    protected $attributes = [
        'vote' => '1',
        'id_voter' => '0',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->storage = new \SplObjectStorage();
    }

    public function getAttribute($key)
    {
        return $this->attributes[$key];
    }

    public function setAttribute($key, $value)
    {
        $this->attributes[$key] = $value;
    }

    public function attach(\SplObserver $observer)
    {
        $this->storage->attach($observer);
    }

    public function detach(\SplObserver $observer)
    {
        $this->storage->detach($observer);
    }

    public function notify()
    {
        foreach ($this->storage as $observer)
        {
            $observer->update($this);
        }
    }

    /**
     * @param $request
     */
    public function createVote($request): void
    {
        $arrayRequest = $request->all();
        $this->notify();
        $this->voteRepository = new Repository(new self());
        $this->voteRepository->create(['id_candidate' => $arrayRequest['candidate_id']]);
    }
}
