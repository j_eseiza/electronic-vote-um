<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * App\Voter
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $dni
 * @property string $address
 * @property string $status
 * @property string $email
 * @property string $birthday
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voter whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voter whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voter whereDni($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voter whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voter whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voter whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voter whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Voter whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 */
class Voter extends Model
{
    use Notifiable;

    protected $fillable = [
        'first_name', 'last_name','birthday','dni','address','status','email',
    ];

    public function canVote(): bool
    {
        return ($this->status !== '1');
    }

    /**
     * @param $voter_dni
     * @return Model|null|object|static
     */
    public function findVoterByDNI($voter_dni){
        return Voter::whereDni($voter_dni)->first();
    }
}
