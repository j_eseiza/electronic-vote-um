<?php

namespace App\Observers;

use App\Notifications\VoterNotification;
use App\Voter;
use Illuminate\Support\Facades\Notification;
use SplSubject;

class VoteObserver implements \SplObserver
{
    public function update(SplSubject $subject)
    {
        $idVoter = $subject['id_voter'];
        $voter = Voter::find($idVoter);
//        Notification::send($voter, new VoterNotification());
    }
}