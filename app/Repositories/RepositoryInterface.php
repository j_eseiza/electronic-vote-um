<?php

namespace App\Repositories;

interface RepositoryInterface
{
    public function getAll();
    public function getAllPaginated(int $paginate);
    public function create(array $data);
    public function update(array $data, int $id);
    public function delete(int $id);
    public function show(int $id);
    public function findById(int $id);
    public function findByFilter($filter, $param);
}