<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class Repository implements RepositoryInterface
{
    private $model;

    /**
     * Repository constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->model->orderBy('last_name', 'asc')->get();
    }

    /**
     * @param int $paginate
     * @return mixed
     */
    public function getAllPaginated(int $paginate)
    {
        return $this->model->orderBy('last_name', 'asc')
            ->paginate($paginate);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function findById(int $id){
        return $this->model->find($id);
    }

    /**
     * @param $filter
     * @param $param
     * @return mixed
     */
    public function findByFilter($filter, $param)
    {
        return $this->model->where($filter, 'like', '%' . $param . '%')->paginate(10);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function show(int $id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * @param array $data
     * @param int $id
     * @return mixed
     */
    public function update(array $data, int $id)
    {
        $model = $this->model->find($id);
        $model->update($data);
        return $model;
    }

    /**
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return $this->model->destroy($id);
    }
}