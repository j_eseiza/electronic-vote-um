<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class VoterDNIRequest extends  FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'dni'=>'required|digits_between:7,8|numeric' ,
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'dni.required' => 'El DNI es un campo necesario',
            'dni.min' => 'El DNI debe ser de al menos 7 digitos',
        ];
    }
}