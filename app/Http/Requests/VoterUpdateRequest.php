<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class VoterUpdateRequest extends  FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'first_name'=>'required|string',
            'last_name'=>'required|string',
            'birthday'=>'required|string',
            'dni'=>'required|digits_between:7,8|numeric',
            'address'=>'required|string',
            'email'=>'required'
        ];
    }
}