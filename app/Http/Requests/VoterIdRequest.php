<?php

namespace App\Http\Requests;

use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Foundation\Http\FormRequest;

class VoterIdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        return [
            'voter_id'=>'required' ,
        ];
    }
    
    public function messages()
    {
        return [
            'voter_id.required' => 'El DNI es un campo necesario',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $messages = $validator->messages();
            foreach ($messages->all() as $message) {
                Toastr::error($message);
            }
        });
    }
}
