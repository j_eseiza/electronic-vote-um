<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VoterRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'first_name'=>'required|alpha',
            'last_name'=>'required|alpha',
            'birthday'=>'required|string',
            'dni'=>'required|digits_between:7,8|numeric',
            'address'=>'required|string',
            'email'=>'required|unique:voters',
        ];
    }
}