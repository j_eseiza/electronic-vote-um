<?php

namespace App\Http\Controllers;

use App\Http\Requests\VoterRequest;
use App\Http\Requests\VoterUpdateRequest;
use App\Repositories\Repository;
use App\Voter;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class VoterController extends Controller
{
    private $voterRepository;

    public function __construct(Voter $voter)
    {
        $this->middleware('auth');
        $this->voterRepository = new Repository($voter);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function index()
    {
        $voters = $this->voterRepository->getAllPaginated(10);
        return view('voter.list_voter', compact('voters'));
    }

    /**
     * @return View
     */
    public function create()
    {
        return view('voter.register_voter');
    }

    /**
     * @param VoterRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(VoterRequest $request)
    {
        if (! $this->verifyAge($request->input('birthday'))) {

            return back()->with('age', 'Menor de edad');
        }

        $arrayVoter = $request->all();
        $arrayVoter['status'] = 0;
        $this->voterRepository->create($arrayVoter);

        Toastr::success('Se agrego votante correctamente', 'Votante');

        return redirect()->route('votante.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function edit($id)
    {
        $voter = $this->voterRepository->findById($id);

        return view('voter.edit_voter', compact('voter'));
    }

    /**
     * @param VoterUpdateRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(VoterUpdateRequest $request, $id)
    {
        $this->voterRepository->update($request->all(), $id);

        Toastr::success('Se edito correctamente', 'Votante');

        return redirect()->route('votante.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|RedirectResponse|View
     */
    public function searchByFilter(Request $request)
    {
        if ($request->filter === 'Filtro' || $request->search === null){
            Toastr::warning('Revise los parametros de busqueda', 'Buscar');

            return redirect()->route('votante.index');
        }

        $voters = $this->voterRepository->findByFilter($request->filter, $request->search);
        if ($voters->isEmpty()){
           Toastr::info('No se encontraron coincidencias');
           
           return redirect()->route('votante.index');
       }

        return view('voter.list_voter', compact('voters'));
    }

    public function voterIndex(): View
    {
        $voter = $this->voterRepository->getAll();

        return view('principal', ['voter' => $voter]);
    }

    private function verifyAge($date): bool
    {
        $arrayDate = date_parse($date);
        $age = Carbon::createFromDate($arrayDate['year'],$arrayDate['month'], $arrayDate['day'])->age;

        return ($age > 18);
    }
}