<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Repositories\Repository;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;


class CandidateController extends Controller
{
    /**
     * @var Candidate
     */
    private $candidate;

    private $candidateRepository;

    public function __construct(Candidate $candidate)
    {
        $this->middleware('auth')->except(['listCandidates', 'searchWinnerCandidate']);
        $this->candidate = new Candidate();
        $this->candidateRepository = new Repository($candidate);
    }

    public function listCandidates(): Collection
    {
        return $this->candidateRepository->getAll();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function index()
    {
        $candidates = $this->candidateRepository->getAll();

        return view('candidate.list_candidate', compact('candidates'));
    }

    /**
     * @return View
     */
    public function create()
    {
        $politicalParties = $this->candidate->getPoliticalParties();

        return view('candidate.register_candidate', compact('politicalParties'));
    }

    /**
     * @param Request $request
     * @return $this|RedirectResponse
     */
    public function store(Request $request)
    {
        $arrayCandidate = $request->all();
        $validationRules = $this->candidate->getValidationRules();

        $validator = Validator::make($arrayCandidate, $validationRules);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $imageName = $this->saveImage($request);
        $arrayCandidate['image'] = $imageName ?? 'default.jpg';

        $this->candidateRepository->create($arrayCandidate);

        Toastr::success('Se agregó candidato correctamente', 'Candidato');

        return redirect()->route('candidato.index');
    }

    /**
     * @param $id
     * @return View
     */
    public function edit($id)
    {
        $candidate = $this->candidateRepository->findById($id);
        $politicalParties = $this->candidate->getPoliticalParties();

        return view('candidate.edit_candidate',[
            'candidate' => $candidate,
            'political_parties' => $politicalParties,
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this|RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $arrayCandidate = $request->all();

        $validationRules = $this->candidate->getValidationRules();
        $validationRules['email'] = 'required|email';
        $validationRules['dni'] = 'required|digits_between:7,8|numeric';

        $messages = $this->candidate->getValidationMessages();

        $validator = Validator::make($arrayCandidate, $validationRules, $messages);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $imageName = $this->saveImage($request);
        $arrayCandidate['image'] = $imageName ?? 'default.jpg';

        $this->candidateRepository->update($arrayCandidate, $id);

        Toastr::info('Se edito correctamente', 'Candidato');

        return redirect()->route('candidato.index');
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $candidate = $this->candidateRepository->findById($id);

        if ($candidate->getAttribute('first_name') === 'WhiteVote'){
            Toastr::warning('Este candidato no se puede eliminar');

            return back();
        }

        $this->candidateRepository->delete($id);

        Toastr::success('Se elimino correctamente', 'Candidato');

        return redirect()->route('candidato.index');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function searchByFilter(Request $request)
    {
        if ($request->filter === 'Filtro' || $request->param === null){
            Toastr::warning('Revise los parametros de busqueda', 'Buscar');

            return redirect()->route('candidato.index');
        }

        $candidates = $this->candidateRepository->findByFilter($request->filter, $request->param);

        if ($candidates->isEmpty()){
            Toastr::info('No se encontraron coincidencias');

            return redirect()->route('candidato.index');
        }

        return view('candidate.list_candidate', compact('candidates'));
    }

    private function saveImage(Request $request): ?string
    {
        $fileName = null;

        if (isset($request->image)) {
            $file = $request->file('image');
            $fileName= $file->getClientOriginalName();
            $file->storeAs('',$fileName, 'candidates');
        }

        return $fileName;
    }

    public function candidatesIndex(): View
    {
        $candidates =  $this->candidateRepository->getAll();

        return view('principal', ['candidates' => $candidates]);
    }

    public function searchWinnerCandidate(): View
    {
        $candidate = $this->candidate->getWinnerCandidate();
        $candidate = $this->candidateRepository->findById($candidate->id);

        // Cambio en config el status de la votacion
        session(['voting_status' => false]);

        return view ('candidate.winner_view', compact('candidate'));
    }
}
