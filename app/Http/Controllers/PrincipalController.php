<?php

namespace App\Http\Controllers;

use App\Http\Requests\VoterDNIRequest;
use App\Http\Requests\VoterIdRequest;
use App\Observers\VoteObserver;
use App\Repositories\Repository;
use App\Voter;
use App\Vote;

class PrincipalController extends Controller
{
    private $voter;
    private $vote;
    private $voterRepository;
    private $voteRepository;

    public function __construct(Voter $voter)
    {
        $this->voter = new Voter();
        $this->vote = new Vote();
        $this->voterRepository = new Repository($this->voter);
        $this->voteRepository = new Repository(new Vote());
    }

    public function index()
    {
        if (session('voting_status') !== null) {
            return redirect()->action('CandidateController@searchWinnerCandidate');
        }
        return view('principal');
    }

    /**
     * @param VoterIdRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createVoteAndUpdateStatusVoter(VoterIdRequest $request)
    {
        $this->vote->attach(new VoteObserver());
        $this->vote->createVote($request);
        $voter = $this->updateVoterStatus($request);

        return view('voters.notifications.vote_ticket', ['voter' => $voter]);
    }

    /**
     * @param VoterDNIRequest $request
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Http\JsonResponse|null|object|static
     */
    public function voterSearch(VoterDNIRequest $request)
    {
        $voter_dni = $request->dni;
        $voter = $this->voter->findVoterByDNI($voter_dni);

        if (empty($voter)) {
            return response()->json([
                'errors' =>[
                    'message' =>'No se encontro votante'
                ]
            ], 500);
        }

        if (! $voter->canVote()) {
            return response()->json([
                'errors' =>[
                    'message' =>'Usted ya ha votado'
                ]
            ], 500);
        }

        return $voter;
    }

    /**
     * @param $request
     * @return mixed
     */
    private function updateVoterStatus($request)
    {
        return $this->voterRepository->update([
           'status' => '1'
        ], $request->input('voter_id'));
    }
}
