<?php

namespace Tests\CandidateTests;

use App\Candidate;
use App\Voter;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CandidateTest extends TestCase
{
    /**
     * @return void
     */
    public function testCandidateStore()
    {
        $candidate = factory(Candidate::class)->make()->toArray();
        $this->post(route('candidate.store'), $candidate)
            ->assertStatus(201)
            ->assertJson($candidate);
    }
}
